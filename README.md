# Meteor_Landing_Analysis

### Group Members:

    - Ted Donegan  (tdonegan@nd.edu)
    - Greg Gavenda (ggavenda@nd.edu)

### JS Web Client:

    Start the server from within the server directory:
        $ python3 server.py
    
    Open the public page and test out the client
