console.log('page load - entered main.js for js-other api');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered get form info');
    var mid = document.getElementById("id-text").value;
    makeNetworkCallToMeteorApi(mid);
}

function makeNetworkCallToMeteorApi(mid){
    console.log('entered makeNetworkCallToAgeApi');
    var url = "http://localhost:51058/meteors/" + mid;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = function(e){
        console.log('network response received' + xhr.responseText);
        updateLocationWithResponse(name, xhr.responseText);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null);
}

function updateLocationWithResponse(mid, response_text){
    console.log('updating page with meteor API response');
    console.log(response_text)

    var response_json = JSON.parse(response_text);

    var label_item = document.getElementById("response-line1");

    if(response_json['location'] == null) {
      label_item.innerHTML = "No meteor with ID " + mid + " was found.";
      label_item = document.getElementById("response-line2");
      label_item.innerHTML = "";
    } else {
      label_item.innerHTML = "Meteor " + mid + " landed in " + response_json['location'];
      makeNetworkCallToTeleportApi(response_json['location']);
    }
}

function makeNetworkCallToTeleportApi(location){
    console.log('enetered makeNetworkCallToNumbersApi');
    var url = "https://api.teleport.org/api/cities/?search=" + location;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = function(e) {
        console.log('got nw response from numbersapi');
        updateCountryWithResponse(location, xhr.responseText);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null);
}

function updateCountryWithResponse(location, response_text){
    console.log('updating page with Teleport API response');

    var response_json = JSON.parse(response_text);
    var label_item = document.getElementById("response-line2");

    try{
      var country = response_json['_embedded']['city:search-results'][0]['matching_full_name'];
      label_item.innerHTML = "Location: " + country;
    }
    catch(err){
      label_item.innerHTML = "Unable to find respective country for " + location;
    }
}
