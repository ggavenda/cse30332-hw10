import cherrypy
import re, json
from meteor_library import _meteor_database

class ResetController(object):

	def __init__(self, mdb=None):
		if mdb is None:
			self.mdb = _meteor_database()
		else:
			self.mdb = mdb

	def PUT_INDEX(self):
		# reset all meteor data
		output = {'result' : 'success'}

		try:
			self.mdb.__init__()
			self.mdb.load_meteors('meteors.dat')

		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)

		return json.dumps(output)

	def PUT_KEY(self, meteor_id):
		# reset meteor's data based on meteor id
		output = {'result' : 'success'}

		try:
			mid = int(meteor_id)
			mdbtmp = _meteor_database()
			mdbtmp.load_meteors('meteors.dat')
			meteor = mdbtmp.get_meteor(mid)
			self.mdb.set_meteor(mid, meteor)

		except Exception as ex:
			output['result'] = 'error'
			output['message'] = str(ex)

		return json.dumps(output)
