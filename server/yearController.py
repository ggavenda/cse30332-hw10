import cherrypy
import re, json
from meteor_library import _meteor_database

class YearController(object):

	def __init__(self, mdb=None):
		if mdb is None:
			self.mdb= _meteor_database()
		else:
			self.mdb = mdb

	def GET_KEY(self, year):
        # return all meteors from a certain year
		output = {'result' : 'success'}

		try:
			output['year'] = int(year)
			output['locations'] = self.mdb.get_year(year)

		except Exception as ex:
			output['result'] = 'failure'
			output['message'] = str(ex)

		return json.dumps(output)

	def GET_INDEX(self):
		# return all meteors sorted into years
		output = {'result' : 'success'}

		try:
			year_buckets = self.mdb.get_all_years()
			for year, meteors in sorted(self.mdb.get_all_years().items()):
				year = int(year)
				output[year] = meteors

		except Exception as ex:
			output['result'] = 'failure'
			output['message'] = str(ex)

		return json.dumps(output)
